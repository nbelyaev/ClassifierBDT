import argparse
import ConfigParser
import os
import ObsConstruct
import SeparationFilter
import CorrelationFilter

parser = argparse.ArgumentParser(description='Steering script for observables creation and selection')
parser.add_argument('config',
    help='Name of the config file to be used')
#parser.add_argument('--create', action='store_true',
#    help='Create the observables dictionary')
#parser.add_argument('--sep', action='store_true',
#    help='Create the list of observables separation metrics')
#parser.add_argument('--sel', action='store_true',
#    help='Select the observables that pass the separation metric criterion')
#parser.add_argument('--cor', action='store_true',
#    help='Select the observables that pass the correlation criterion')

args = parser.parse_args()

config = ConfigParser.ConfigParser()
config.read(args.config)

#reading the main info from the config file
signalFilePath = config.get('MAIN', 'SIGNAL_FILE_PATH')
backgroundFilePath = config.get('MAIN', 'BACKGROUND_FILE_PATH')
outputFolder = config.get('MAIN', 'OUTPUT_FOLDER')

jetNum = int(config.get('CONSTRUCTION', 'JET_NUM'))
bosonNum = int(config.get('CONSTRUCTION', 'BOSON_NUM'))

generateObservables = config.get('MAIN', 'GENERATE_OBSERVABLES')
doSeparation        = config.get('MAIN', 'DO_SEPARATION')
doSelection         = config.get('MAIN', 'DO_SELECTION')
doCorrelation       = config.get('MAIN', 'DO_CORRELATION')

if not os.path.exists(outputFolder):
    os.makedirs(outputFolder) 

#values that may repeat in the chain
outputFileNameBase = str()
initialDictPath = str()
metricListFilePath = str()

#Creating the observables dictionary
if generateObservables:
    fsList = eval(config.get('CONSTRUCTION', 'FS_LIST'))
    outputFileNameBase = config.get('CONSTRUCTION', 'OUT_FILE_NAME_BASE')

    createdDictName = outputFileNameBase + 'Dict.py'
    createdDictPath = os.path.join(outputFolder, createdDictName)

    ObsConstruct.ObsConstruct(createdDictPath, fsList)
    initialDictPath = createdDictPath

#Creating the list of the separation metrics
if doSeparation:
    if not generateObservables:
        initialDictPath = config.get('SEPARATION', 'INPUT_DICT_NAME')
        outputFileNameBase = config.get('SEPARATION', 'OUT_FILE_NAME_BASE')
        
    useMultiprocessing = config.getboolean('SEPARATION', 'USE_MULTIPROC')
    metricList = SeparationFilter.ComputeMetricList(jetNum, bosonNum, signalFilePath, backgroundFilePath, initialDictPath, useMultiprocessing)

    metricListFileName = outputFileNameBase + 'Metrics.txt'
    SeparationFilter.SaveMetricList(metricList, outputFolder, metricListFileName)

    #for the 'Selection' part of the program, if present
    metricListFilePath = os.path.join(outputFolder, metricListFileName)

#Selecting observables based on their separation metric
if doSelection:
    if not generateObservables and not doSeparation:
    
        initialDictPath = config.get('SEPARATION', 'INPUT_DICT_NAME')
        outputFileNameBase = config.get('SEPARATION', 'OUT_FILE_NAME_BASE')

    if not doSeparation:
        metricListFilePath = config.get('SEPARATION', 'IN_METRIC_LIST_NAME')

    metric = config.get('SEPARATION', 'METRIC')
    metricMin = float(config.get('SEPARATION', 'METRIC_MIN'))
    passedObservablesList = SeparationFilter.SelectObservables(metricListFilePath, metric, metricMin)

    outputFileNameBase = outputFileNameBase + metric + str(metricMin).replace('.', '')
    outputListName = outputFileNameBase + 'Metrics.txt'
    SeparationFilter.SaveMetricList(passedObservablesList, outputFolder, outputListName)

    outputDictName = outputFileNameBase + 'Dict.py'
    SeparationFilter.SaveObservablesDict(jetNum, bosonNum, [obsName for obsName, _ in passedObservablesList], outputFolder, outputDictName)

    #for the 'Correlation' part of the program, if present
    initialDictPath = os.path.join(outputFolder, outputDictName)

#Selecting observables based on their correlation coefficient
if doCorrelation:
    if not generateObservables and not doSeparation and not doSelection:
        initialDictPath = config.get('CORRELATION', 'INPUT_DICT_NAME')
        outputFileNameBase = config.get('CORRELATION', 'OUT_FILE_NAME_BASE')    

    correlationMax = float(config.get("CORRELATION", 'CORRELATION_MAX'))
    metric = config.get('CORRELATION', 'METRIC')

    print initialDictPath, correlationMax
    metricList = CorrelationFilter.CorrelationFilter(jetNum, bosonNum, signalFilePath, backgroundFilePath, initialDictPath, correlationMax, metric)

    outputFileNameBase = outputFileNameBase + 'Cor' + str(correlationMax).replace('.', '') + metric
    
    outputListName = outputFileNameBase + 'Metrics.txt'
    SeparationFilter.SaveMetricList(metricList, outputFolder, outputListName)

    outputDictName = outputFileNameBase + 'Dict.py'
    SeparationFilter.SaveObservablesDict(jetNum, bosonNum, [obsName for obsName, _ in metricList], outputFolder, outputDictName)