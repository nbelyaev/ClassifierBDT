import os
import sys
import ROOT
import Operators
import itertools
import collections
import ConfigParser
from tqdm import tqdm
from ROOT import *

def combinations_with_replacement(iterable, r):
    # combinations_with_replacement('ABC', 2) --> AA AB AC BB BC CC
    pool = tuple(iterable)
    n = len(pool)
    if not n and r:
        return
    indices = [0] * r
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != n - 1:
                break
        else:
            return
        indices[i:] = [indices[i] + 1] * (r - i)
        yield tuple(pool[i] for i in indices)

def ObsConstruct(createdDictPath, fsList):
    print "=======> Start building observables..."
    fsListStr = str(fsList)

    fOut = open(createdDictPath,"w+")
    fOut.write("import ROOT\n")
    fOut.write("from ROOT import TMath\n")
    fOut.write("\n")
    fOut.write("#Names of the observables we want to have in the reprocessed files\n")
    varN = 0
    vars = []

    combList = []
    tmp = fsList[:1]
    tmp.append("-"+fsList[0])
    print "=======> Constructing list of simple objects..."
    for comb in tqdm(range(2, len(fsList)+1)):
        tmp.append(fsList[comb-1])
        combList.extend(list(combinations_with_replacement(tmp, 2)))
        for pair in combList:
                for op, func in Operators.opsInitial.items():
                    if fsList[comb-1] in func(pair[0], pair[1]) and pair[0] != pair[1]:
                        tmp.append(func(pair[0], pair[1]))
        
    fsList = tmp
    #print "After linear combinations:"
    #print fsList
    combList = list(combinations_with_replacement(fsList, 2))
    #print "\n Comblist:"
    #print combList
            
    scalarValues = []
    dotValues = []    
    
    print "=======> Applying unary operators to simple objects..."
    for particle in tqdm(fsList):
        for op, func in Operators.opsSingle.items(): 
            varN += 1
            scalarValues.append(func(particle))
            vars.append(func(particle))
            
    combList = list(combinations_with_replacement(fsList, 2))
    #print "\n After dot combinations:"
    #print fsList
    print "=======> Applying unary operators to complex objects..."
    for pair in tqdm(combList):
        for op, func in Operators.opsSingle.items(): 
            for opt, funct in Operators.opsSingle.items(): 
                if func(pair[0]) != funct(pair[1]):
                    varN += 1
                    vars.append(func(pair[0]) + "/" + funct(pair[1]))                  
   
    print "=======> Applying binary operators to simple objects..."
    for pair in tqdm(combList):
        for op, func in Operators.opsDouble.items():
            if pair[0] != pair[1]:
                varN += 1
                vars.append(func(pair[0], pair[1]))
                dotValues.append(func(pair[0], pair[1]))
    
    print "=======> Applying binary operators to complex objects..."    
    for pair in tqdm(combList):
        for pairt in combList:
            for op, func in Operators.opsDouble.items():
                if func(pair[0], pair[1]) != func(pairt[0], pairt[1]):
                    varN += 1
                    vars.append("(" + func(pair[0], pair[1]) + ")/(" + func(pairt[0], pairt[1]) + ")")

    print "=======> Applying scalar product operator to simple objects..." 
    for element in tqdm(scalarValues):
        for elementt in dotValues:
            if element != elementt:
                varN += 1
                vars.append("(" + element + ")/(" + elementt + ")")

    print "=======> Applying scalar product operator to complex objects..." 
    for element in tqdm(dotValues):
        for elementt in scalarValues:
            if element != elementt:
                varN += 1
                vars.append("(" + element + ")/(" + elementt + ")")            
                   
    print "=======> Observables have been built."
    print "=======> " + str(varN) + " observables have been constructed."
    print "=======> Start writing variables on disk..."

    fOut.write("\n")
    fOut.write("#A function, that take few particles' four-vectors as an input and \n")
    fOut.write("#calculates the observable based on the provided key (obsName)\n")
    fOut.write("observablesDict = {\n")
    for func in tqdm(vars): 
        fOut.write("    '{0:100s} (lambda {1:<s}: {2:<s} \n".format(str(func)+"':", fsListStr[1:-1].replace('\'', ''), format(str(func))+"),"))
    fOut.write("    }\n\n")
    fOut.close()
    
    print "=======> Raw observables dictionary has been created."