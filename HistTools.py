import ROOT 
import os
from array import array

# Extensions for the saved histograms and corresponging subfolders
OutputTypes = [
    (".png", None),
    (".pdf", "pdf/"),
    (".root", "root/"),
]

def PlotVariableFromTree(tree, variableName, histName, binning=None, addOverflow=True, cut=''):
    '''
    Creates a histogram from a branch in a tree or chain of trees

    By default it normalizes the plot, adds overflow to the last bin and underflow to the first bin 
    
    Parameters:
        tree (TTree or TChain): tree or chain of trees containing the variable
        variableName (str): name of the variable
        histName (str): name of the created histogram
        binning (str): binning of the created histogram. Should be formatted 'number_of_bins,min_value,max_value'
        addOverflow (bool): if true, adds overflow to the last bin and underflow to the first bin
        cut (str): acts as a cut option for TTree::Draw(), i.e. passing criteria if result is bool, entry weight 
            if result is float

    Returns:
        TH1
    '''
    drawOptions = '{var}>>{hist}'.format(var=variableName, hist=histName)
    if binning:
        drawOptions += '({binning})'.format(binning=binning)
    tree.Draw(drawOptions, cut, 'gOff')
    hist = ROOT.gDirectory.Get(histName)
    if addOverflow:
        AddBins(hist, hist.GetNbinsX(), hist.GetNbinsX() + 1) #Overflow
        AddBins(hist, 1, 0) #Underflow
    hist.Scale(1 / hist.GetSumOfWeights ())

    return hist

def PlotVariableFromTreeCustomBinning(tree, variableName, histName, binList, binNum, addOverflow=True, cut=''):
    '''
    Creates a histogram from a branch in a tree or chain of trees

    By default it normalizes the plot, adds overflow to the last bin and underflow to the first bin 
    
    Parameters:
        tree (TTree or TChain): tree or chain of trees containing the variable
        variableName (str): name of the variable
        histName (str): name of the created histogram
        binList (list of float): list of the bins' left edges. Should have binNum+1 entries
        binNum (int): number of bins in the histogram
        addOverflow (bool): if true, adds overflow to the last bin and underflow to the first bin
        cut (str): acts as a cut option for TTree::Draw(), i.e. passing criteria if result is bool, entry weight 
            if result is float

    Returns:
        TH1
    '''

    hist = ROOT.TH1D(histName, histName, binNum, array('d', binList))
    drawOptions = '{var}>>{hist}'.format(var=variableName, hist=histName)
    tree.Draw(drawOptions, cut, 'gOff')
    if addOverflow:
        AddBins(hist, hist.GetNbinsX(), hist.GetNbinsX() + 1) #Overflow
        AddBins(hist, 1, 0) #Underflow
    hist.Scale(1 / hist.GetSumOfWeights ())
    
    return hist

def PlotVariableFromList(eventObservableList, histName, binNum, obsMin, obsMax, addOverflow=True):
    '''
    Creates a histogram from a list of (observable, weight) tuples

    By default it normalizes the plot, adds overflow to the last bin and underflow to the first bin 
    
    Parameters:
        eventObservableList (list of (observable, weight) tuples): value of the observable
            and the weight of the corresponding event
        histName (str): name of the created histogram
        binNum (int): number of bins in the histogram
        obsMin (float): minimum value of the histogram
        obsMax (float): maximum value of the histogram
        addOverflow (bool): if true, adds overflow to the last bin and underflow to the first bin

    Returns:
        TH1
    '''

    hist = ROOT.TH1D(histName, histName, binNum, obsMin, obsMax)
    
    entryNum = len(eventObservableList)
    observablesArray = array('d', [observable for observable, weight in eventObservableList]) 
    weightArray = array('d', [weight for observable, weight in eventObservableList])
    hist.FillN(entryNum, observablesArray, weightArray)

    if addOverflow:
        AddBins(hist, hist.GetNbinsX(), hist.GetNbinsX() + 1) #Overflow
        AddBins(hist, 1, 0) #Underflow
    
    hist.Scale(1 / hist.GetSumOfWeights ())
   

    return hist

def PlotVariableFromListCustomBinning(eventObservableList, histName, binList, binNum, addOverflow=True):
    '''
    Creates a histogram from a branch in a tree or chain of trees

    By default it normalizes the plot, adds overflow to the last bin and underflow to the first bin 
    
    Parameters:
        eventObservableList (list of (observable, weight) tuples): value of the observable
            and the weight of the corresponding event
        histName (str): name of the created histogram
        binList (list of float): list of the bins' left edges. Should have binNum+1 entries
        binNum (int): number of bins in the histogram
        addOverflow (bool): if true, adds overflow to the last bin and underflow to the first bin

    Returns:
        TH1
    '''

    hist = ROOT.TH1D(histName, histName, binNum, array('d', binList))
    
    entryNum = len(eventObservableList)
    observablesArray = array('d', [observable for observable, weight in eventObservableList]) 
    weightArray = array('d', [weight for observable, weight in eventObservableList])
    hist.FillN(entryNum, observablesArray, weightArray)

    if addOverflow:
        AddBins(hist, hist.GetNbinsX(), hist.GetNbinsX() + 1) #Overflow
        AddBins(hist, 1, 0) #Underflow
    hist.Scale(1 / hist.GetSumOfWeights ())
    
    return hist

def AddBins(hist, targetBin, addedBin):
    '''
    Adds content of one bin of the histogram to another

    Parameters:
        hist (TH1): histogram
        targetBin (int): number of the bin we want to add to
        addedBin (int): number of the bin we want to add
    Retrurns
        Sets content of targetBin to the sum of contet of two bins
        Sets error of the targetBin to the square root of sum of squares of error of two bins
    '''
    initialValue = hist.GetBinContent(targetBin)
    initialError = hist.GetBinError(targetBin)

    addedValue = hist.GetBinContent(addedBin)
    addedError = hist.GetBinError(addedBin)

    hist.SetBinContent(targetBin, initialValue + addedValue)
    hist.SetBinError(targetBin, ROOT.TMath.Sqrt(initialError**2 + addedError**2))

    hist.SetBinContent(addedBin, 0)
    hist.SetBinError(addedBin, 0)


def SaveCanvas(canvas, basePath, name=None):
    '''
    Saves the canvas with all of the extensions and subfolders

    Creates folder if they weren't created before

    Parameters:
        canvas (TCanvas): canvas we want to save
        basePath (str): path to the main folder we want to save canvas to
        name (str): name of the saved canvas
    Returns:
        Saves the canvas with the extensions and subfolders specified in OutputTypes
    '''
    if not name:
        name = canv.GetName()

    for extension, subPath in OutputTypes:
        outputPath = basePath
        if subPath:
            outputPath = os.path.join(outputPath, subPath)
        
        if not os.path.exists(outputPath):
                os.makedirs(outputPath) 
        
        outputPath = os.path.join(outputPath, name + extension)

        canvas.SaveAs(outputPath)

def GetMaxHist(histograms):
    '''
    Finds histogram with the highest peak value

    Parameters:
        histograms (list of TH1)
    Returns
        TH1: histogram with the highest peak value
    '''
    maxHist = None
    peak = None
    for hist in histograms:
        if hist.GetMaximum > peak or peak == None:
            peak = hist.GetMaximum()
            maxHist = hist
    return maxHist

def GetLegend(entryList, position=None):
    '''
    Creates a legend that shouldn't overlap with the histograms

    Parameters:
        entryList (list of ntuples (TH1, str, str)): ntuple containing histogram, title of the
            legend entry and the marker type
        position (list of float): position of the legend formatted 'xLeft,yBottom,xRight,yTop'
    Returns:
        (TLegend): legend with entries for signal and background with fill boxes
    '''

    legend = None
    #Setting legend position
    if position:
        legend = ROOT.TLegend(position[0], position[1], position[2], position[3])
    else:
        xLeft = 0    
        xRight = 0
        yBottom = 0.75
        yTop = 0.9

        histograms = [entry[0] for entry in entryList]
        maxHist = GetMaxHist(histograms)
        if(maxHist.GetNbinsX() / 2. < maxHist.GetMaximumBin()):
            xLeft = 0.1
            xRight = 0.4
        else:
            xLeft = 0.6
            xRight = 0.9
        legend = ROOT.TLegend(xLeft, yBottom, xRight, yTop)
    #Setting legend content
    for histogram, entryTitle, entryMarker in entryList:
        legend.AddEntry(histogram, entryTitle, entryMarker)
    return legend 