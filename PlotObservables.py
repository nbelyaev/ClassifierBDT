#coding=utf8
'''
Module for creating the plots of the observables

    *Normalizes all of the histograms (default for HistTools.PlotobservableFromTree)
    *Adds overflow to the last bin and the underflow to the first bin
    *Extensions and subfolders are specified in HistTools.OutputTypes
'''
import ROOT 
import imp
import HistTools
from TreeCreator import MakeBranchName

def PlotObservables(signalFileList, backgroundFileList, observableList, outputFolder):
    '''
    Plots the input observables stored in the specified files with drawing options

    Parameters:
        signalFileList (list): list of the paths to the signal samples containing the observables stored in the 'observablesTree' tree
        backgroundFileList (list): list of the paths to the background samples containing the observables stored in the 'observablesTree' tree
        observableList (list of tuples (str, str, str)): list of 
            *observable names. Should be the same as in the dictionary that was used to create signal and background trees
            *binning, formatted 'binNum,obsMin,obsMax'. Could be 'None'
            *x-axis label. Colud be 'None'
        outputFolder (srt): path to the folder where the plots would be saved

    Returns:
        Plots of the observables in the output folder with the extensions and subfolders specified in the OutputTypes
    '''
    print '=======> Plotting the input observables' 

    signalChain = ROOT.TChain("observablesTree")
    backgroundChain = ROOT.TChain("observablesTree")

    for signalFile in signalFileList:
        signalChain.Add(signalFile)

    for backgroundFile in backgroundFileList:
        backgroundChain.Add(backgroundFile)

    for observableName, binning, axisTitle in observableList:

        branchName = MakeBranchName(observableName)
        #Making histograms
        signalHistName = branchName + 'Signal'
        signalHist = HistTools.PlotVariableFromTree(signalChain, branchName, signalHistName, binning, cut='weight')

        backgroundHistName = branchName + 'Background'
        backgroundHist = HistTools.PlotVariableFromTree(backgroundChain, branchName, backgroundHistName, binning, cut='weight')

        #Creating canvas
        canvasName = branchName + 'Canvas'
        canvas = ROOT.TCanvas(canvasName, canvasName)

        #Setting histogram displaying options
        histMax = 1.1 * max(signalHist.GetMaximum(), backgroundHist.GetMaximum())
        signalHist.SetMaximum(histMax)

        if not axisTitle:
            axisTitle = observableName

        signalHist.GetXaxis().SetTitle(axisTitle)
        signalHist.SetLineWidth(2)
        signalHist.SetFillColor(ROOT.kBlue - 10)
        signalHist.SetStats(0)
        signalHist.SetTitle('')
        signalHist.Draw('hist')

        backgroundHist.SetLineWidth(2)
        backgroundHist.SetLineColor(ROOT.kRed)
        backgroundHist.SetFillColor(ROOT.kRed)
        backgroundHist.SetFillStyle(3354)
        backgroundHist.Draw('samehist')

        #Creating histogram legend
        legendEntryList = [(signalHist, 'Signal', 'f'), 
            (backgroundHist, 'Background', 'f')]
        legend = HistTools.GetLegend(legendEntryList)
        legend.Draw('same')

        #Saving the histogram
        HistTools.SaveCanvas(canvas, outputFolder, observableName)


def main():
    signalFileList = ['output/Reproc_Signal.root']
    backgroundFileList = ['output/Reproc_Background.root']

    #how this could be automated
    dictFilePath = 'output/GitTest/Cor07kolmogorovDict.py'
    _obsDict = imp.load_source('ObsDict', dictFilePath)
    observablesDict = _obsDict.observablesDict

    observableListAuto = [(obsName, None, None) for obsName in observablesDict.keys()]
    outputFolderAuto = 'output/GitTest/Observables/Auto'
    PlotObservables(signalFileList, backgroundFileList, observableListAuto, outputFolderAuto) 
        
    #how the values could be set manualy
    observableListManual = [
    ('(jet1 - jet2).M()', '20,-3000,0', 'm(j1-j2) [GeV]' ), 
    ('(jet1 - boson1 + boson2).E()', '20,-2000,3000,', 'E(j1) + E(j2) + E_{T}^{miss} [GeV]'),
    ]

    outputFolderManual = 'output/GitTest/Observables/Manual'
    PlotObservables(signalFileList, backgroundFileList, observableListManual, outputFolderManual)

if __name__ == "__main__":
    main()