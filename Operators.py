import ROOT
import collections
from ROOT import TMath

def vectorX( vec1 ):
    return  "("+ vec1 + ").X()"
    
def vectorY( vec1 ):
    return  "("+ vec1 + ").Y()"
    
def vectorZ( vec1 ):
    return  "("+ vec1 + ").Z()"
   
def vectorT( vec1 ):
    return  "("+ vec1 + ").T()"
    
def vectorPx( vec1 ):
    return  "("+ vec1 + ").Px()"
    
def vectorPy( vec1 ):
    return  "("+ vec1 + ").Py()"

def vectorPz( vec1 ):
    return  "("+ vec1 + ").Pz()"
 
def vectorE( vec1 ):
    return  "("+ vec1 + ").E()"
    
def vectorPt( vec1 ):
    return  "("+ vec1 + ").Pt()"
    
def vectorEta( vec1 ):
    return  "("+ vec1 + ").Eta()"
    
def vectorPhi( vec1 ):
    return  "("+ vec1 + ").Phi()"
    
def vectorM( vec1 ):
    return  "("+ vec1 + ").M()"
    
def vectorP( vec1 ):
    return  "("+ vec1 + ").P()"
   
def vectorRho( vec1 ):
    return  "("+ vec1 + ").Rho()"  
    
def vectorTheta( vec1 ):
    return  "("+ vec1 + ").Theta()" 
    
def vectorAbs( vec1 ):
    return "ROOT.TMath.Abs(" + vec1 + ")"

def vectorAdd( vec1, vec2 ):
    return vec1 + " + " + vec2
    
def vectorDiff( vec1, vec2 ):
    return vec1 + " - " + vec2
    
def vectorScalProd( vec1, vec2 ):
    return "(" + vec1 + ")*(" + vec2 + ")"

def vectorVecProd( vec1, vec2 ):
    return vec1 + ".Vect().Cross(" + vec2 + ".Vect())"
    
opsInitial = collections.OrderedDict([('vectorAdd',      vectorAdd),
                                      ('vectorDiff',     vectorDiff)])
                                     
opsDouble  = collections.OrderedDict([('vectorScalProd', vectorScalProd)])

opsSingle  = collections.OrderedDict([#('vectorX',        vectorX), 
                                      #('vectorY',        vectorY),
                                      #('vectorZ',        vectorZ),
                                      #('vectorT',        vectorT),
                                      ('vectorPx',       vectorPx),
                                      ('vectorPy',       vectorPy),
                                      ('vectorPz',       vectorPz),
                                      ('vectorE',        vectorE),
                                      ('vectorPt',       vectorPt),
                                      ('vectorEta',      vectorEta),
                                      ('vectorPhi',      vectorPhi),
                                      ('vectorM',        vectorM),
                                      #('vectorRho',      vectorRho),
                                      #('vectorTheta',    vectorTheta),
                                      #('vectorAbs',      vectorAbs),
                                      ('vectorP',        vectorP)])