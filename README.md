ClassifierBDT V. 1.0.
This is a initial version of the QCD/VBF TMVA-based BDT classifier.
To run the classifier, use the following syntax:

python Classifier.py 

Configuration file config.ini contains the parameters of the code.
Here, SIGNAL_SAMPLE, BACKGROUND_SAMPLE and DATA_SAMPLE are the paths to the initial signal, background and data samples, respectively.
Input files must contain four TTrees - vPx, vPy, vPz and vE. 
Each of these branches must have two values per event, corresponds to two final state jets. 

The options REPROCESSING and CLASSIFICATION control the TTrees reprocessing and classification.
In case of REPROCESSING = YES, TTrees with new observables will be reprocessed from the base input TTrees.
After the reprocessing, four branches from initial files will be transformed into new branches, correspond to chosen observables.
In case of REPROCESSING = NO, the reprocessing will not be launched.
Parameter CLASSIFICATION have the same controll keys.

Script Classifier.py is the main function of this classifier.
Script TreeCreator.py perform the TTrees reprocessing.
Script ObsDict.py contains and calculates the observables.

ROOT 6.12.X or higher and Python 2.7.Y is required for correct work.