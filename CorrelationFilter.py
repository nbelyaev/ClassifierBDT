import time
import imp
from ROOT import TFile, TMath
from SeparationFilter import GetEventLorentzList, GetMetricDict

def GetEventListMean(eventList):
	'''
	Calculates the weighted mean value of the observable

	Parameters:
		eventList (list of (float, float)): list of observables and event weights
	Returns:
		(float)
	'''
	numerator = sum([(observable * weight) for observable, weight in eventList])
	denominator = sum(weight for _, weight in eventList)
	return numerator / denominator

def GetEventListRootSqrErr(eventList):
	'''
	Calculates the root of the weighted sum of squares of the deviation from the mead 

	Parameters:
		eventList (list of (float, float)): list of observables and event weights
	Returns:
		(float)
	'''
	mean = GetEventListMean(eventList)
	weightedSquaresSum = sum([(weight * (observable - mean) ** 2) for observable, weight in eventList])
	return weightedSquaresSum ** 0.5 

def GetBestObservable(obsDict, metric):
	'''
	Finds the observables with the highest value of the metric (and thus provides the best separation)

	Parameters:
		obsDict (dict {string: {string: float}}): dictionary of observable names and their metrics
		metric (str): name of the metric being used. Should be 'chi2' or 'kolmogorov'
	Returns
		(tuple (string, {string: float})): name of the best observable with the highes metric and its' metrics
	'''
	metricMax = 0
	bestObs = tuple()
	for obsName, metricDict in obsDict.iteritems():
		if metricDict[metric] > metricMax:
			metricMax = metricDict[metric]
			bestObs = (obsName, metricDict)
	return bestObs

def CorrelationFilter(jetNum, bosonNum, signalFilePath, backgroundFilePath, dictFilePath, correlationLimit, metric):
	'''
	Looks for observables with the absolute value of the correlation coefficient higher than the limit and selects the best separating one,
	based on the specified metric

	Parameters:
		jetNum (int): number of jets in the event
		bosonNum (int): number of bosons in the event
		signalFilePath (string): path to the file containing signal events
		backgroundFilePath (string): path to the file containing background events
		dictFilePath (string): path to the file containing dictionary with observable names and functions to compute the observables
		correlationLimit (float): the observables with the absolute value of the correlation coefficient higher than this limit a considered correlated
		metric (string): name of the metric to find the best separating observable. Should be 'kolmogorov' or 'chi2'
	Returns:
		list of (obsName, {'metric': value}) tuples. Metrics are 'chi2' and 'kolmogorov'
	'''
	start = time.time()

	#creating lists of particles 4-vectors and corresponding event weights
	signalFile = TFile(signalFilePath, 'read')
	signalEventLorentzList = GetEventLorentzList(signalFile, jetNum=jetNum, bosonNum=bosonNum)

	backgroundFile = TFile(backgroundFilePath, 'read')
	backgroundEventLorentzList = GetEventLorentzList(backgroundFile, jetNum=jetNum, bosonNum=bosonNum)	

	#sum of all weights is necessary to calculate the equal probability binning
	#calculating it here, so we don't have to do it for every observable
	weightTotalSum = sum(weight for _, weight in signalEventLorentzList)

	#using ordered list for more efficient calculations
	_obsDict = imp.load_source('ObsDict', dictFilePath)
	observablesDict = _obsDict.observablesDict
	observableList = [(name, function) for name, function in observablesDict.iteritems()]

	observableNameSkipList = list()
	passedObsCandidateList = list()
	obsNum = len(observableList)

	for obsXIdx, obsX in enumerate(observableList): #we need the index for the inner cycle
		obsXName, obsXFunction = obsX[0], obsX[1]
		print '{0} observable out of {1}'.format(obsXIdx + 1, obsNum)

		#if we haven't found this observable redundant earlier in the cycle
		if obsXName not in observableNameSkipList:
			correlatedObsDict = dict()

			signalObsXList = [(obsXFunction(*lorentzVectorTuple), weight) for lorentzVectorTuple, weight in signalEventLorentzList]
			obsXMean = GetEventListMean(signalObsXList)
			obsXRootSqrError = GetEventListRootSqrErr(signalObsXList)

			#cycle over the observables that are AFTER the obsX in the list
			for obsYName, obsYFunction in observableList[obsXIdx+1:]:

				#if we haven't found this observable redundant earlier in the cycle
				if obsYName not in observableNameSkipList:
					signalObsYList = [(obsYFunction(*lorentzVectorTuple), weight) for lorentzVectorTuple, weight in signalEventLorentzList] 
					obsYMean = GetEventListMean(signalObsYList)
					obsYRootSqrError = GetEventListRootSqrErr(signalObsYList)
		
					correlationCoeffNumerator = 0
					for i in range(len(signalObsXList)): #choice between obsX and obsY lists is arbitrary because they have the same length
						#the data is stored in tuples (observable value, event weight)
						correlationCoeffNumerator += signalObsXList[i][1] * (signalObsXList[i][0] - obsXMean) * (signalObsYList[i][0] - obsYMean)

					correlationCoeff = correlationCoeffNumerator / (obsXRootSqrError * obsYRootSqrError)
					
					if TMath.Abs(correlationCoeff) > correlationLimit:		
						print 'Detected correlation of {} with {}, corr = {}'.format(obsXName, obsYName, correlationCoeff)
						backgroundObsYList = [(obsYFunction(*lorentzVectorTuple), weight) for lorentzVectorTuple, weight in backgroundEventLorentzList]
						metricDict = GetMetricDict(signalObsYList, backgroundObsYList, obsYName, weightTotalSum)

						correlatedObsDict[obsYName] = metricDict

			if len(correlatedObsDict) > 0:

				#the inner cycle doesn't check correlation of the observable with itself
				backgroundObsXList = [(obsXFunction(*lorentzVectorTuple), weight) for lorentzVectorTuple, weight in backgroundEventLorentzList]
				metricDict = GetMetricDict(signalObsXList, backgroundObsXList, obsXName, weightTotalSum)
				correlatedObsDict[obsXName] = metricDict

				#finding the best separating observable out of correlated ones
				bestObs = GetBestObservable(correlatedObsDict, metric)
				bestObsName = bestObs[0]
				print '{} is the best variable out of {}'.format(bestObsName, ', '.join(list(correlatedObsDict.keys())))
				correlatedObsDict.pop(bestObsName)

				#it is only a candidate for now, because it could be found to be redundant later in the cycle	
				if bestObs not in passedObsCandidateList:
					passedObsCandidateList.append(bestObs)

				#the rest of the correlated observables won't be considered from now on
				for obsName in correlatedObsDict.keys():
					observableNameSkipList.append(obsName)

	#creating the final list of the best performing non-correlated variables
	passedObservablesList = list()
	for obsName, metricDict in passedObsCandidateList:
		if obsName not in observableNameSkipList:
			passedObservablesList.append((obsName, metricDict))

	print time.time() - start
	return passedObservablesList