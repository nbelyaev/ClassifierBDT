import os
import sys
import imp
import ROOT
import ConfigParser
from array import array
import SeparationFilter

config = ConfigParser.ConfigParser()
config.read('config.ini')

def SplitTree(file, treeName):
    '''
    Creates two trees filled with even and odd events

    Parameters:
        file (TFile)
        treeName (string)
    '''
    tree = file.Get(treeName)

    outputTreeList = [tree.CloneTree(0), tree.CloneTree(0)] #0 -- number of copied events
    outputTreeList[0].SetName(treeName + '_even')
    outputTreeList[1].SetName(treeName + '_odd')

    for entryIdx in range(tree.GetEntries()):
        tree.GetEntry(entryIdx)
        outputTreeList[entryIdx % 2].Fill()

    for outputTree in outputTreeList:
        outputTree.Write()

    file.Close()

def MakeBranchName(name):
    '''
    Makes an apropriate branch name without illegal symbols

    Parameters:
        name (string)
    Returns:
        (string)
    '''
    replaceTuple = (('(', ''), 
        (')', ''), 
        ('.', ''), 
        (' + ', 'Plus'), 
        (' - ', 'Minus'),
        ('/', 'Over'),
        ('*', 'Times'))
    for r in replaceTuple:
        name = name.replace(*r)
    return name

def processTrees(names=["vPx", "vPy", "vPz", "vE"]):
    jetNum = 2
    bosonNum = 0
    dictFilePath = 'output/fullChainTestkolmogorov02Cor02kolmogorovDict.py'

    print "=======> Initializing TTree reprocessor..."
    print "=======> Reading branches " + str(names) + "..."

    #Opening the files that will be reprocessed
    #These files contain only four branches, indicated in "names" list
    Signal      = ROOT.TFile(config.get('MAIN', 'SIGNAL_SAMPLE'), "read")
    Background  = ROOT.TFile(config.get('MAIN', 'BACKGROUND_SAMPLE'), "read")
    Data        = ROOT.TFile(config.get('MAIN', 'DATA_SAMPLE'), "read")
    fileList    = {Signal:"Signal", Background:"Background", Data:"Data"}

    print "=======> Creating input TTrees..."
    #Creating new files, that will be used for classification
    #These files will contain new branches, which correspond to the observables indicated in ObsDict.py
    for file in fileList: #Looping over the files (signal, background and data)
        #temporary solution for the weight check
        hasWeightTree = True
        if fileList[file] == 'Data':
            hasWeightTree = False

        #reading the input file
        fName = str(file)[21:-16] #Extracting the filename from the description of the TFile object
        print "=======> File {} is being reprocessed...".format(fName)
        inputJetsTree = file.Get("jetsTree")
        inputBosonTree = file.Get("bosonTree")
        if inputJetsTree.GetEntries() != inputBosonTree.GetEntries(): 
            raise ValueError("The number of events is different in jets and boson TTrees! Check consistency of the samples!")
       
        inputWeightTree = ROOT.TTree()
        if hasWeightTree:
            inputWeightTree = file.Get('weightTree')

        #creating output file and tree
        outputReprDir = config.get('MAIN', 'REPROC_OUTPUT_DIR')
        if not os.path.exists(outputReprDir): 
            os.makedirs(outputReprDir)      
        outputFile = ROOT.TFile(outputReprDir + "Reproc_" + fileList[file] + ".root", "recreate")
        observablesTree = ROOT.TTree("observablesTree", "observablesTree")

        #adding branches with observables to the output tree
        observableFuncValueList = list()
        branchList = list()

        _obsDict = imp.load_source('ObsDict', dictFilePath)
        observablesDict = _obsDict.observablesDict
        obsIdx = 0
        for obsName, obsFunction in observablesDict.iteritems(): #Looping over the observables we want to construct
            print "         Adding observale {}".format(obsName)
            value = array('f', [0.]) #Creating a branch variable
            observableFuncValueList.append((obsFunction, value))
            branchName = MakeBranchName(obsName)
            branch = observablesTree.Branch(branchName, observableFuncValueList[obsIdx][1], branchName + "/F") #Creating the branch itself
            branchList.append(branch)
            obsIdx += 1

        #adding the weight branch
        weight = array('f', [1])
        weightBranch = observablesTree.Branch('weight', weight, 'weight/F')

        #computing the observables and copying the weighs (if present)
        print "         Computing observables..."
        for entryIdx in range(inputJetsTree.GetEntries()): #Looping over the events in input trees
            jetTuple = SeparationFilter.GetParticleTuple(inputJetsTree, jetNum, entryIdx)
            if ((jetTuple[0].E() == -jetTuple[1].E()) or 
                (jetTuple[0].Px() == jetTuple[1].Px() and jetTuple[0].Py() == jetTuple[1].Py()) or 
                (jetTuple[0].Eta() == jetTuple[1].Eta())): 
                continue
            bosonTuple = SeparationFilter.GetParticleTuple(inputBosonTree, bosonNum, entryIdx)

            for obsFunc, obsValue in observableFuncValueList:
                obsValue[0] = obsFunc(*(jetTuple + bosonTuple))

            if hasWeightTree:
                inputWeightTree.GetEntry(entryIdx)
                weight[0] = inputWeightTree.weight

            observablesTree.Fill()
        
        observablesTree.SetEntries()
        observablesTree.Write() 

        SplitTree(outputFile, 'observablesTree')

        outputFile.Close() #Close the output file
        print "=======> Done"
    print "=======> Input TTees have been created!"

if __name__ == '__main__':
    processTrees()