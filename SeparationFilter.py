import time
import imp
import os
from operator import itemgetter
from ROOT import TFile, TLorentzVector, TCanvas, kRed, kBlue
import HistTools

def IsClose(a, b, rel_tol=1e-09, abs_tol=0.0):
    '''
    Compares two float numbers
    '''
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

def GetParticleTuple(tree, particleNum, entryIdx):
    '''
    Creates a tuple of the 4-vectors of the particles in the specified event

    Parameters:
        tree (TTree): tree with the particles momentum and energy
        particleNum (int): number of the particles in the event
        entryIdx (int): number of the event
    Returns:
        Tuple of TLorentzVector
    '''
    tree.GetEntry(entryIdx)
    particleTuple = tuple()
    for particleIdx in range(particleNum):
        particle = TLorentzVector()
        particle.SetPxPyPzE(tree.vPx[particleIdx], tree.vPy[particleIdx], tree.vPz[particleIdx], tree.vE[particleIdx])
        particleTuple += (particle, )

    return particleTuple

def GetEventLorentzList(file, jetNum=0, bosonNum=0, hasWeightTree=True):
    '''
    Computes 4-vector of the particles in the file and the weights of the corresponding events

    Parametres:
        file (TFile): input file. Should have at least one of the following TTrees: jetTree, bosonTree.
            if hasWeightTree=True the file should also have weightTree TTree
        jetNum (int): number of the jets in every event
        bosonNum (int): number of the bosons in every event
        hasWeightTree (bool): whether the input file has a TTree with event weights
    Returns:
        list of tuples (tuple of TLorentzVector, weight)
    '''
    if jetNum != 0:
        jetTree = file.Get('jetsTree')
    if bosonNum != 0:
        bosonTree = file.Get('bosonTree')
    weightTree = file.Get('weightTree')

    eventLorentzList = list()

    for entryIdx in range(jetTree.GetEntries()): #supposing we have jets in our trees
        jetTuple = tuple()
        if jetNum != 0:
            jetTuple = GetParticleTuple(jetTree, jetNum, entryIdx)
        bosonTuple = tuple()
        if bosonNum != 0:
            bosonTuple = GetParticleTuple(bosonTree, bosonNum, entryIdx)

        weight = 1
        if hasWeightTree:
            weightTree.GetEntry(entryIdx)
            weight = weightTree.weight

        eventLorentzList.append((jetTuple + bosonTuple, weight))

    return eventLorentzList

def MakeEqualProbBinning(observableTupleList, observableMin, observableMax, binNum, weightTotalSum):
    '''
    Calculates bin width that make for an equal number of entries of target tree in each bin

    Parameters:
        observableTupleList (list of tuples (tuple of TLorentzVector, weight))
        observableMin (float):
        observableMax (float):
        binNum (int): desired number of bins in the histogram
        weightTotalSum (float): sum of the wights of all the events
    
    Returns:
        (list of float): array of left edges for each bin. This is an array of size binNum+1
    '''

    #zero entry of the array of bins left-edges is the variable minimum 
    binLeftEdgeList = [observableMin]

    observableTupleList.sort(key=itemgetter(0))

    #Calculating bins left edges
    partNum = 1
    weightPartSum = 0
    for observable, weight in observableTupleList:
        weightPartSum += weight

        if weightPartSum > (weightTotalSum / binNum) * partNum :
            binLeftEdgeList.append(observable)
            partNum += 1

            if partNum >= binNum:
                break

    #binNumber + 1 entry of the array of bins left-edges is the variable maximum
    binLeftEdgeList.append(observableMax)

    return binLeftEdgeList

def GetMetricDict(signalEventObservableList, backgroundEventObservableList, obsName, weightTotalSum, binNum=20, saveHistogram=False, outputFolder='output/'):
    '''
    Computes chi2/Ndf and Kolmogorov distance for specified observable

    Parameters:
        signalEventObservableList (list of tuples (tuple of TLorentzVector, weight)):
        backgroundEventObservableList (list of tuples (tuple of TLorentzVector, weight))
        obsName (string): name of the observable
        weightTotalSum (float): sum of the wights of all the events
        binNum (int): number of bins in the histogram used to compute chi2 and Kolmogorov distance
        saveHistogram (bool): flag indicating if we want to save the histograms used to compute chi2 and Kolmogorov distance
        outputFolder (str): output folder for the histograms

    Returns:
        (dict {'metric': value}): metrics are 'chi2' and 'kolmogorov'
    '''

    observableMin = min(min(signalEventObservableList, key=itemgetter(0)), min(backgroundEventObservableList, key=itemgetter(0)))[0]
    observableMax = max(max(signalEventObservableList, key=itemgetter(0)), max(backgroundEventObservableList, key=itemgetter(0)))[0]
    
    #dirty way to deal with the distributuins that have the same minimum and maximum value
    if IsClose(observableMin, observableMax): 
        observableMax = observableMax + 1
        observableMin = observableMin - 1

    binLeftEdgeList = MakeEqualProbBinning(signalEventObservableList, observableMin, observableMax, binNum, weightTotalSum)

    signalHistName = '{var}Signal'.format(var=obsName)
    signalHist = HistTools.PlotVariableFromListCustomBinning(signalEventObservableList, signalHistName, binLeftEdgeList, binNum)

    backgroundHistName = '{var}Background'.format(var=obsName)
    backgroundHist = HistTools.PlotVariableFromListCustomBinning(backgroundEventObservableList, backgroundHistName, binLeftEdgeList, binNum)

    kolmogorov = signalHist.KolmogorovTest(backgroundHist, 'NM')
    chi2 = signalHist.Chi2Test(backgroundHist, 'CHI2/NDF')

    if saveHistogram:

        canvasName = '{var}Canvas'.format(var=obsName)
        canvas = TCanvas(canvasName, canvasName)

        histMax = 1.05 * max(signalHist.GetMaximum(), backgroundHist.GetMaximum())
        signalHist.SetMaximum(histMax)
        histMin =  0.95 * min(signalHist.GetMinimum(), backgroundHist.GetMinimum())
        signalHist.SetMinimum(0)

        signalHist.GetXaxis().SetTitle(obsName)
        signalHist.SetLineWidth(2)
        signalHist.SetFillColor(kBlue - 10)
        signalHist.SetStats(0)
        signalHist.SetTitle('')
        signalHist.Draw('hist')

        backgroundHist.SetLineWidth(2)
        backgroundHist.SetLineColor(kRed)
        backgroundHist.SetFillColor(kRed)
        backgroundHist.SetFillStyle(3354)
        backgroundHist.Draw('samehist')

        legendEntryList = [(signalHist, 'Signal', 'f'), 
            (backgroundHist, 'Background', 'f')]
        legend = HistTools.GetLegend(legendEntryList)
        legend.Draw('same')

        fileName = obsName.replace('/', 'Over')
        HistTools.SaveCanvas(canvas, outputFolder, fileName)

    return {'chi2': chi2, 'kolmogorov': kolmogorov}

def ComputeMetricListSubprocess(observablesList, signalEventLorentzList, backgroundEventLorentzList, weightTotalSum):
    '''
    Computes the metrics of the given observables

    Parameters:
        observalbesList (list of (string, lambda)): list of observables
        signalEventObservableList (list of tuples (tuple of TLorentzVector, weight)):
        backgroundEventObservableList (list of tuples (tuple of TLorentzVector, weight))
        obsName (string): name of the observable
        weightTotalSum (float): sum of the wights of all the events
    Returns:
        list of (obsName, {'metric': value}) tuples. Metrics are 'chi2' and 'kolmogorov'
    '''
    #print '-subprocess started', current_process().pid
    
    results = list()
    for observableName, observableFunction in observablesList:

        signalEventObservableList = [(observableFunction(*lorentzVector), weight) for lorentzVector, weight in signalEventLorentzList]
        backgroundEventObservableList = [(observableFunction(*lorentzVector), weight) for lorentzVector, weight in backgroundEventLorentzList]

        metricDict = GetMetricDict(signalEventObservableList, backgroundEventObservableList, observableName, weightTotalSum, saveHistogram=False)

        results.append((observableName, metricDict))
    #print '-subprocess ended', current_process().pid 
    
    return results

def SplitList(inputList, partsNum):
    '''
    Splits the list in 'partNum' (almost) equal parts

    Parameters:
        inputList (list): list to be splitted
        partsNum (int): number of parts
    Returns:
        (list of lists)
    '''
    baseLen, rem = divmod(len(inputList), partsNum)
    return [inputList[i * baseLen + min(i, rem) : (i + 1) * baseLen + min(i + 1, rem)] for i in range(partsNum)]

def ComputeMetricList(jetNum, bosonNum, signalFilePath, backgroundFilePath, dictFilePath, useMultiprocessing=False):
    '''
    Computes the separation metrics for the given observables

    Parameters:
        jetNum (int): number of jets in the event
        bosonNum (int): number of bosons in the event
        signalFilePath (string): path to the file containing signal events
        backgroundFilePath (string): path to the file containing background events
        dictFilePath (string): path to the file containing dictionary with observable names and functions to compute the observables
        useMultiprocessing (bool): if true, the observables list is splitted to nCPU parts that are processed in parallel
    Returns:
        list of (obsName, {'metric': value}) tuples. Metrics are 'chi2' and 'kolmogorov'

    '''

    start = time.time()

    #creating lists of particles 4-vectors and corresponding event weights
    signalFile = TFile(signalFilePath, 'read')
    signalEventLorentzList = GetEventLorentzList(signalFile, jetNum=jetNum, bosonNum=bosonNum)

    backgroundFile = TFile(backgroundFilePath, 'read')
    backgroundEventLorentzList = GetEventLorentzList(backgroundFile, jetNum=jetNum, bosonNum=bosonNum)

    #sum of all weights is necessary to calculate the equal probability binning
    #calculating it here, so we don't have to do it for every observable
    weightTotalSum = sum(weight for _, weight in signalEventLorentzList)

    #converting the dictionary to the list so we can split it for parallel calculations
    _obsDict = imp.load_source('ObsDict', dictFilePath)
    observablesDict = _obsDict.observablesDict
    observablesList = [(name, function) for name, function in observablesDict.iteritems()]

    #calculating the separation metrics of the observables
    metricList = list()    
    if useMultiprocessing == True:
        from multiprocess import Pool, cpu_count

        processNum = cpu_count()
        splittedObservableLists = SplitList(observablesList, processNum)

        pool = Pool(processes=processNum)
        results = [pool.apply_async(ComputeMetricListSubprocess, args=(obsList, signalEventLorentzList, backgroundEventLorentzList, weightTotalSum))
			for obsList in splittedObservableLists]
        pool.close()
        pool.join()
        for r in results:
            metricList += r.get()
    else:
        metricList = ComputeMetricListSubprocess(observablesList, signalEventLorentzList, backgroundEventLorentzList, weightTotalSum)

    #saving the list of the observables and their separation metrics
    print time.time() - start
    return metricList

def SaveMetricList(metricList, outputFolder, name):
    '''
    Creates a .txt file with a list of observables name and their metrics

    Paremeters: 
        metricList (list of (string, {string: float})): list of observables and their metrics. Keys: 'kolmogorov', 'chi2'
        outputFolder (string): folder where the .txt file with metrics should be saved
        name (string): name of the file (including .txt extension)

    Creates outputFolder/metrics.txt file with observables names and metrics
    '''
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder) 

    outputPath = os.path.join(outputFolder, name)
    file = open(outputPath, 'w')    
    variableString = '\n'.join(['{var}\t{chi}\t{kol}'.format(var=obsName, chi=metricDict['chi2'], kol=metricDict['kolmogorov']) 
        for obsName, metricDict in metricList])
    file.write(variableString)
    file.close()

def SaveObservablesDict(jetNum, bosonNum, obsNameList, outputFolder, name):
    '''
    Creates a .py file with the {'obsName': lamda} dictionary

    Parameters:
        jetNum (int): number of the jets in every event
        bosonNum (int): number of the bosons in every event
        obsNameList (list of str): names of te observables
        outputFolder (string): folder where the .txt file with metrics should be saved
        name (string): name of the file (including .py extension)
    '''
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder) 

    outputFilePath = os.path.join(outputFolder, name)
    file = open(outputFilePath, 'w')
    headerString = 'from ROOT import TLorentzVector, TMath\n\nobservablesDict = {\n'
    
    lambdaVariableList = list()
    for jetIdx in range(jetNum):
        lambdaVariableList.append('jet{0}'.format(jetIdx + 1))
    for bosonIdx in range(bosonNum):
        lambdaVariableList.append('boson{0}'.format(bosonIdx + 1))    
    lambdaVariableString = ', '.join(lambdaVariableList)

    obsString = ''.join(['\t\'{obs}\':\t\t\t\t(lambda {lambdaVar}: {obs}),\n'.format(obs=obsName, lambdaVar=lambdaVariableString) for obsName in obsNameList])
    file.write(headerString + obsString + '}')
    file.close()

def SelectObservables(metricFileName, metricName, metricMin):
    '''
    Selects observables from .txt files that pass the metric criterion and returns list of their names and metrics

    Parameters:
        metricFileName (str): name of the file that contains the data in the format
            observableName chi2/Ndf D_n
        metricName (str): name of the metric we use to set the limit. Should be 'chi2' or 'kolmogorov'
        metricMin (float): minumum value of the metric
        
    Returns:
        list of (obsName, {'metric': value}) tuples. Metrics are 'chi2' and 'kolmogorov'
    '''
    metricFile = open(metricFileName, 'r')

    passedObservablesList = list()

    totalObservablesNum = 0
    passedObservablesNum = 0

    for line in metricFile:
        totalObservablesNum += 1
        lineList = line.split('\t')
        metricDict = dict()
        observableName, metricDict['chi2'], metricDict['kolmogorov'] = lineList[0], float(lineList[1]), float(lineList[2]) 

        if metricDict[metricName] > metricMin:
            passedObservablesNum += 1
            passedObservablesList.append((observableName, metricDict))

    print '{metric} > {metricMin}, {ratio:3.2f} % of the variables passed'.format(metric=metricName, metricMin=metricMin, ratio=float(passedObservablesNum)/float(totalObservablesNum)*100)

    return passedObservablesList